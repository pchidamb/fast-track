/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
/* Desc: A 4-wheeled vehicle
 * Author: Nate Koenig
 */

#ifndef _GAZEBO_TRACKCONTROL_PLUGIN_HH_
#define _GAZEBO_TRACKCONTROL_PLUGIN_HH_

#include <string>
#include <vector>

#include "gazebo/common/Plugin.hh"
#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"
#include "gazebo/util/system.hh"

struct State{
	// position and orientation defined at center of bucket
	double time,xpos, ypos, zpos,roll, pitch,yaw,ImplStatus;

	State() :time(0), xpos(0), ypos(0), zpos(0), roll(0),pitch(0), yaw(0),ImplStatus(0) {}
};
namespace gazebo
{
  class GAZEBO_VISIBLE TrackControlPlugin : public ModelPlugin
  {
    /// \brief Constructor
    public: TrackControlPlugin();

    public: virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
    public: virtual void Init();
    private: void OnUpdate();
    private: void Reset();
    
    private: std::vector<event::ConnectionPtr> connections;
    private: std::vector<physics::JointPtr> joints;
    private: std::vector<physics::LinkPtr> links;
   	private: physics::WorldPtr World;
    private: physics::ModelPtr model;
    private: transport::NodePtr node;
   	private: double deltaSimTime=0, timePrevious=0;
   	private: State hex;
   	private: std::vector<State> history;
   	private: transport::PublisherPtr pub;
   	private: msgs::Pose msg;
    private: double forwardSpeed = 1.0;
    private: double rotationSpeed = 0.25;
    private: double CircleSpeed =  0.5;
    private: double *r=(double*)malloc (sizeof(double));
    private: double *p=(double*)malloc (sizeof(double));
    private: double *y=(double*)malloc (sizeof(double));
    private: double StartPos_x=0,StartPos_y=0,StartPos_z=0;
    private: double StartPos_roll=0,StartPos_pitch=0,StartPos_yaw=0;

    
    private: double CurrentPos_x=0,CurrentPos_y=0,CurrentPos_z=0;
    private: double CurrentPos_roll=0,CurrentPos_pitch=0,CurrentPos_yaw=0;
    
    private: double frontdrive =0.0,rotatedrive=0.0,rotatedriveC=0.0,circledrive=0.0,circledriveC=0.0,stairdrive=0.0,obstacledrive=0.0;

    //private: gazebo::math::Pose StartPose,CurrentPose;
	//private: math::Vector3 StartPosition;
    //private: math:: Quaternion StartOrientation;
  };
}
#endif
