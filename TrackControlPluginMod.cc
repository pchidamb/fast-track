/*
 * Copyright (C) 2012 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#include "gazebo/physics/physics.hh"
#include "gazebo/transport/transport.hh"
#include "TrackControlPluginMod.hh"
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>
using namespace std;

# define pi 3.14
# define OUTNAME "HexTrackPath.csv"

using namespace gazebo;
using namespace std;
GZ_REGISTER_MODEL_PLUGIN(TrackControlPlugin)





  static void toEulerAngle(math:: Quaternion q, double& roll, double& pitch, double& yaw)
{
	// roll (x-axis rotation)
	double sinr = +2.0 * (q.w * q.x + q.y * q.z);
	double cosr = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
	roll = atan2(sinr, cosr);

	// pitch (y-axis rotation)
	double sinp = +2.0 * (q.w * q.y - q.z * q.x);
	if (fabs(sinp) >= 1)
		pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
	else
		pitch = asin(sinp);

	// yaw (z-axis rotation)
	double siny = +2.0 * (q.w * q.z + q.x * q.y);
	double cosy = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);  
	yaw = atan2(siny, cosy);
}

TrackControlPlugin::TrackControlPlugin()
{
  // Size the joints and Links Correctly
  this->joints.resize(2);
  this->links.resize(3);
  printf(" Starting ....Great !\n");
}

/////////////////////////////////////////////////
void TrackControlPlugin::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{

  this->model = _model;
  this->World = this->model->GetWorld();
  this->links[0]= model->GetLink(_sdf->Get<std::string>("base_link"));
   if (!this->links[0])
  {
    gzerr << "Unable to find link: base_link AKA base_link\n";
    return;
  }
    this->links[1]= model->GetLink(_sdf->Get<std::string>("left_track"));
   if (!this->links[1])
  {
    gzerr << "Unable to find link: left_track AKA left_track\n";
    return;
  }
    this->links[2]= model->GetLink(_sdf->Get<std::string>("right_track"));
   if (!this->links[2])
  {
    gzerr << "Unable to find link: right_track AKA right_track \n";
    return;
  }
  
// Hex joints - Swivel , Boom , Stick, Bucket
 
  this->joints[0] = this->model->GetJoint(_sdf->Get<std::string>("left_track_j"));
  if (!this->joints[0])
  {
    gzerr << "Unable to find joint: left_track_j AKA left_track_j\n";
    return;
  }

  this->joints[1] = this->model->GetJoint(
      _sdf->Get<std::string>("right_track_j"));

  if (!this->joints[1])
  {
    gzerr << "Unable to find joint: right_track_j AKA right_track_j\n";
    return;
  }

  this->connections.push_back(event::Events::ConnectWorldUpdateBegin(
          boost::bind(&TrackControlPlugin::OnUpdate, this)));
          
  printf("Loading ....Great !\n We Now can control/measure the Excavator\n");

// Load pointers to joints and links of Hex Tracked Machine
;
}



/////////////////////////////////////////////////
void TrackControlPlugin::Init()
{
  // Set Intial state
  // Set up Node
  this->node = transport::NodePtr(new transport::Node());
  this->node->Init(this->model->GetWorld()->GetName());
  // Set up publisher
  pub = this->node->Advertise<msgs::Pose>(
  "~/simple_tracked/cmd_vel");
    // Move the robot somewhere to free space without obstacles.
  this->model->SetWorldPose(ignition::math::Pose3d(10, 10, 0.1, 0, 0, 0));

  
  this->StartPos_x = this->model->WorldPose().Pos().X();
  this->StartPos_y = this->model->WorldPose().Pos().Y();
  this->StartPos_z = this->model->WorldPose().Pos().Z();
  this->StartPos_roll = this->model->WorldPose().Rot().Roll();
  this->StartPos_pitch = this->model->WorldPose().Rot().Pitch();
  this->StartPos_yaw = this->model->WorldPose().Rot().Yaw();
  cout <<" Init complete"<<endl;

}

/////////////////////////////////////////////////
void TrackControlPlugin::OnUpdate()
{	
	if (pub->HasConnections()== false)
		{pub->WaitForConnection();}
		
		
	this->CurrentPos_x = this->model->WorldPose().Pos().X();
	this->CurrentPos_y = this->model->WorldPose().Pos().Y();
	this->CurrentPos_z = this->model->WorldPose().Pos().Z();
	
    this->CurrentPos_roll = this->model->WorldPose().Rot().Roll();
    this->CurrentPos_pitch = this->model->WorldPose().Rot().Pitch();
    this->CurrentPos_yaw = this->model->WorldPose().Rot().Yaw();


   // Test straight driving - 1 sec driving, should move 1 meter forward.
   // check if it covered 1 metre
   // if else print out distance covered and set velcoity to zero
   // else keep driving

   if (sqrt((this->CurrentPos_x - this->StartPos_x)*(this->CurrentPos_x - this->StartPos_x) \
		   +(this->CurrentPos_y - this->StartPos_y)*(this->CurrentPos_y - this->StartPos_y) \
		   +(this->CurrentPos_z - this->StartPos_z)*(this->CurrentPos_z - this->StartPos_z)\
			) < 1.0 && (frontdrive ==0.0))// 1 metre forward
		{
		msgs::Set(&msg, ignition::math::Pose3d(forwardSpeed, 0, 0, 0, 0, 0));
		pub->Publish(msg, true);
		}
	else if (frontdrive ==0.0)
		{
		 cout << "Completed Straight Drive at velocity "<< this->model->GetWorldLinearVel() <<endl;	
		 		 msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, 0));
		 pub->Publish(msg, true);
		 this->World->Reset();
		 msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, rotationSpeed));
         pub->Publish(msg, true);
         frontdrive=1.0;
         timePrevious = this->World->GetSimTime().Double();	
          // reset start position for circle
        this->StartPos_x = this->model->WorldPose().Pos().X();
		this->StartPos_y = this->model->WorldPose().Pos().Y();
		this->StartPos_z = this->model->WorldPose().Pos().Z();
		this->StartPos_roll = this->model->WorldPose().Rot().Roll();
		this->StartPos_pitch = this->model->WorldPose().Rot().Pitch();
		this->StartPos_yaw = this->model->WorldPose().Rot().Yaw();
		}
	
		 
	if ((frontdrive ==1.0) && (rotatedrive==0.0) && (rotatedriveC==0.0))
		{
		 msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, rotationSpeed));
         pub->Publish(msg, true);
         
         if ((abs(this->CurrentPos_yaw - this->StartPos_yaw) <0.001) && ((this->World->GetSimTime().Double() - timePrevious) >1))// back to start and atleast 1 second passed
         {
			 rotatedriveC=1.0;
		 }
		 
		if  ((this->World->GetSimTime().Double() - timePrevious) == 1)
		{	
			cout<<"In 1 second we have moved by yaw angle of "<<this->StartPos_yaw - this->CurrentPos_yaw <<endl;
		}
         	
		}	
	else if ((frontdrive ==1.0) && (rotatedriveC==1.0) && (rotatedrive==0.0))
	{
		cout << "Completed Rotation Drive at velocity "<< this->model->GetWorldAngularVel() <<endl;	;
		msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, 0));
		pub->Publish(msg, true);
		this->World->Reset();
		rotatedrive=1.0;
		msgs::Set(&msg, ignition::math::Pose3d(CircleSpeed, 0, 0, 0, 0, rotationSpeed));
        pub->Publish(msg, true);
        timePrevious = this->World->GetSimTime().Double();	
        
        // reset start position for circle
        this->StartPos_x = this->model->WorldPose().Pos().X();
		this->StartPos_y = this->model->WorldPose().Pos().Y();
		this->StartPos_z = this->model->WorldPose().Pos().Z();
		this->StartPos_roll = this->model->WorldPose().Rot().Roll();
		this->StartPos_pitch = this->model->WorldPose().Rot().Pitch();
		this->StartPos_yaw = this->model->WorldPose().Rot().Yaw();
        
	}
	
	if ((frontdrive ==1.0) && (rotatedrive==1.0) && (circledrive ==0) && (circledriveC ==0))
	
	{
		msgs::Set(&msg, ignition::math::Pose3d(CircleSpeed, 0, 0, 0, 0, rotationSpeed));
        pub->Publish(msg, true);
        
        if ((abs(this->CurrentPos_yaw - this->StartPos_yaw) <0.001) && ((this->World->GetSimTime().Double() - timePrevious) >1))// back to start and atleast 1 second passed
        {
			circledriveC=1.0;
		}
		
		if  ((this->World->GetSimTime().Double() - timePrevious) == 1)
		{	
			cout<<"In 1 second we have moved by yaw angle of "<<this->StartPos_yaw - this->CurrentPos_yaw <<endl;
		}
		
        
	}
		
	else if ((frontdrive ==1.0) && (rotatedrive==1.0) && (circledriveC==1.0) && (circledrive==0.0))
	{
		cout << "Completed Circle Drive at Linear velocity "<< this->model->GetWorldLinearVel() <<endl;
		cout << "Completed Circle Drive at Angular velocity "<< this->model->GetWorldAngularVel() <<endl;
		msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, 0));
		pub->Publish(msg, true);
		this->World->Reset();
		circledrive=1.0;
		timePrevious = this->World->GetSimTime().Double();
	}
	
	// Go up Stairs and Check visually
	
	if ((frontdrive ==1.0) && (rotatedrive==1.0) && (circledrive==1.0) && (stairdrive==0.0))
		{
			
			msgs::Set(&msg, ignition::math::Pose3d(forwardSpeed, 0, 0, 0, 0, 0));
			pub->Publish(msg, true);
			if ( (this->World->GetSimTime().Double() - timePrevious) >3.5)
			{
				stairdrive=1.0;
				cout << "Completed Stair Drive at Linear velocity "<< this->model->GetWorldLinearVel() <<endl;
				cout << "Completed Stair Drive at Angular velocity "<< this->model->GetWorldAngularVel() <<endl;
				msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, 0));
				pub->Publish(msg, true);
				this->World->Reset();
				timePrevious = this->World->GetSimTime().Double();
				const auto beforeCylinderPose = ignition::math::Pose3d(0, -2, 0.1,0, 0, -ignition::math::Angle::HalfPi.Radian());
			    model->SetWorldPose(beforeCylinderPose);
			
			}		
		}
		
	if ((frontdrive ==1.0) && (rotatedrive==1.0) && (circledrive==1.0) && (stairdrive==1.0) && (obstacledrive==0))
		{

			msgs::Set(&msg, ignition::math::Pose3d(forwardSpeed, 0, 0, 0, 0, 0));
			pub->Publish(msg, true);
			if ( (this->World->GetSimTime().Double() - timePrevious) >6)
			{
				obstacledrive=1.0;
				cout << "Completed Obstacle Drive at Linear velocity "<< this->model->GetWorldLinearVel() <<endl;
				cout << "Completed Obstacle Drive at Angular velocity "<< this->model->GetWorldAngularVel() <<endl;
				msgs::Set(&msg, ignition::math::Pose3d(0, 0, 0, 0, 0, 0));
				pub->Publish(msg, true);
				this->World->Reset();
				timePrevious = this->World->GetSimTime().Double();
			}		
		}
	
 
}

void TrackControlPlugin::Reset()
{
;
}

 
  
 
  


  

 
